require 'chefspec/ext'
require 'chefspec'
require 'fileutils'

RSpec.configure do |config|
  config.include Chefspec::Ext

  config.before(:suite) do
    Chef::Config[:config_file] = '/dev/null'
  end
end