require "chefspec/ext/version"
require 'pathname'

module Chefspec
  module Ext
    def spec_path
      @spec_path ||= Pathname.new(CHEFSPEC_ROOT)
    end

    def root_path
      spec_path + ".."
    end

    def cookbook_paths
      [RSpec.configuration.cookbook_path, spec_path + "cookbooks"].map(&:to_s)
    end

    def runner_class
      if defined?(ChefSpec::Runner)
        ChefSpec::Runner
      else
        ChefSpec::ChefRunner
      end
    end

    def runner
      @runner ||= runner_class.new(platform: 'ubuntu', version: '12.04', cookbook_path: cookbook_path.to_s, log_level: :error)
    end

    def runner(opts = {})
      default_opts = {
        platform: 'ubuntu',
        version: '12.04',
        cookbook_path: cookbook_paths,
        log_lever: :error
      }

      @runner ||= runner_class.new(default_opts.merge(opts))
    end

    def converge(recipe, runner_opts={})
      setup_node(runner(runner_opts).node) if defined?(setup_node)
      runner(runner_opts).converge(recipe)
    end

    extend self
  end
end
