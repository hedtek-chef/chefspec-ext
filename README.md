# Chefspec::Ext

A gem that pulls in some helpers to make it easier to write chefspec
tests in a certain way.

## Installation

Add this line to your application's Gemfile:

    gem 'chefspec-ext'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install chefspec-ext

## Usage

Add this line to your spec_helper.rb

    require 'chefspec/ext/rspec'

Alternatively, require the file

    require 'chefspec/ext'

and add this line to any specs you want the extensions in

    include Chefspec::Ext

Within a spec, converge your recipe by simply writing

    chef_run = converge "cookbook_name::recipe"

You can also set extra run options with the second parameter to converge.

E.g. to step into a LWRP for testing, use:

    chef_run = converge "cookbook_name::recipe", step_into: ['my_lwrp']

and then perform your expectations against the chef_run

If you need to add extra node attributes, write a method like the following

    def setup_node(node)
      node.set[:attr] = "foobar"
    end

## BerkshelfShims

If you use BerkshelfShims to set up your cookbook paths for a test, chefspec-ext
will make use of this. When you do

    require 'chefspec/ext/rspec'

berkshelf-shims integration is also added in if available

## Contributing

This project uses git-flow style branches. You don't need to follow all the
naming conventions to use this project, but please make sure you always branch
off the 'develop' branch rather than the master branch, as the develop branch
is always the next release.

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
