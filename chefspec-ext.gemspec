# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'chefspec/ext/version'

Gem::Specification.new do |spec|
  spec.name          = "chefspec-ext"
  spec.version       = Chefspec::Ext::VERSION
  spec.authors       = ["David Workman"]
  spec.email         = ["gems@hedtek.com"]
  spec.description   = %q{Adds a small number of helpers and a certain style of setup to your rspec example groups. The helpers enable a cleaner example group setup without having to repeat certain things across every test (and across all your cookbooks in a large project).}
  spec.summary       = %q{A set of extensions to more easily integrate rspec with chefspec for writing automated chef infrastructure tests}
  spec.homepage      = "https://github.com/hedtek-infra/chefspec-ext"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency 'chefspec', ">= 3.0.0"

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
